package com.example.wedplan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.widget.ListView
import com.android.volley.toolbox.*
import com.android.volley.*
import android.util.Log
import android.content.Intent
import android.widget.AdapterView
import android.widget.TextView

class GuestsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guests)

        val TAG = "GuestsActivity"
//
//        val actionBar = supportActionBar
//        actionBar!!.title = "Guests"

        val apiEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/guests/" + RequestSingleton.getInstance(this).weddingDocKey

        val listView = findViewById<ListView>(R.id.guestsListView)
        listView.dividerHeight = 10

        val guestList = ArrayList<Guest>()

        val request = JsonObjectRequest(Request.Method.GET, apiEndpoint, null,
            Response.Listener { response ->
                Log.i(TAG, response.toString())
                val guestCount = response.get("guest_count")
                findViewById<TextView>(R.id.guestsTotalCount).text = "Total guest count: " + guestCount.toString()
                val guestArray = response.getJSONArray("guests")
                for (i in 0 until guestArray.length()) {
                    val guest = guestArray.getJSONObject(i)
                    guestList.add(Guest(guest.get("fname").toString(), guest.get("lname").toString(), guest.get("plus_one") as Boolean, guest.get("count") as Int))
                }
                val guestAdapter = GuestListViewAdapter(guestList, this)
                listView.adapter = guestAdapter
            },
            Response.ErrorListener { error ->
                Log.d(TAG, error.toString())
            }
        )

        RequestSingleton.getInstance(this).addToRequestQueue(request)

        val addGuestBtn = findViewById<FloatingActionButton>(R.id.guestAddGuest)
        addGuestBtn.setOnClickListener {
            val intent = Intent(this, CreateGuest::class.java)
            intent.putExtra("updateFlag", false)
            startActivity(intent)
        }

        listView.onItemClickListener = AdapterView.OnItemClickListener {
            adapterView, view, i, l ->
            val guest = adapterView.getItemAtPosition(i) as Guest

            val intent = Intent(this, CreateGuest::class.java)
            intent.putExtra("updateFlag", true)
            intent.putExtra("fname", guest.fname)
            intent.putExtra("lname", guest.lname)
            intent.putExtra("plus_one", guest.plus_one)
            intent.putExtra("pos", i)

            startActivity(intent)
        }
    }
}
