package com.example.wedplan

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import android.util.Log
import org.w3c.dom.Text

val TAG = "ListViewAdapters"

class Guest {
    var fname: String = ""
    var lname: String = ""
    var plus_one: Boolean = false
    var count: Int = 1

    constructor() {}

    constructor(fn: String, ln: String, po: Boolean, c: Int) {
        fname = fn
        lname = ln
        plus_one = po
        count = c
    }
}

class Venue {
    var name: String = ""
    var location: String = ""
    var cost: String = ""
    var website: String = ""
    var phone_number: String = ""
    var contact_person: String = ""

    constructor() {}

    constructor(n: String, l: String, c: String, w: String, pn: String, cp: String) {
        name = n
        location = l
        cost = c
        website = w
        phone_number = pn
        contact_person = cp
    }
}

class Task {
    var title: String = ""
    var due_date: String = ""
    var assignee: String = ""
    var description: String = ""

    constructor() {}

    constructor(t: String, d: String, a: String, desc: String) {
        title = t
        due_date = d
        assignee = a
        description = desc
    }
}

class GuestListViewAdapter(items: ArrayList<Guest>, ctx: Context) :
        ArrayAdapter<Guest>(ctx, R.layout.guest, items) {
    private class GuestItemViewHolder {
        internal var name: TextView? = null
        internal var total_count: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView

        val viewHolder: GuestItemViewHolder

        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.guest, parent, false)

            viewHolder = GuestItemViewHolder()
            viewHolder.name = view.findViewById(R.id.guestName) as TextView
            viewHolder.total_count = view.findViewById(R.id.guestCount) as TextView
        }
        else {
            viewHolder = view.tag as GuestItemViewHolder
        }

        val guest = getItem(position)

        viewHolder.name!!.text = guest.fname + " " + guest.lname
        viewHolder.total_count!!.text = "Guest party size: " + guest.count.toString()

        view!!.tag = viewHolder

        return view
    }
}

class TaskListViewAdapter(items: ArrayList<Task>, ctx: Context) :
    ArrayAdapter<Task>(ctx, R.layout.task, items) {
    private class TaskItemViewHolder {
        internal var title: TextView? = null
        internal var date: TextView? = null
        internal var assignee: TextView? = null
        internal var description: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView

        val viewHolder: TaskItemViewHolder

        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.task, parent, false)

            viewHolder = TaskItemViewHolder()
            viewHolder.title = view.findViewById(R.id.taskTitle) as TextView
            viewHolder.date = view.findViewById(R.id.taskDueDate) as TextView
            viewHolder.assignee = view.findViewById(R.id.taskAssignee) as TextView
            viewHolder.description = view.findViewById(R.id.taskDescription) as TextView
        }
        else {
            viewHolder = view.tag as TaskItemViewHolder
        }

        val task = getItem(position)

        viewHolder.title!!.text = task.title
        viewHolder.date!!.text = task.due_date
        viewHolder.assignee!!.text = task.assignee
        viewHolder.description!!.text = task.description

        view!!.tag = viewHolder

        return view
    }
}

class VenueListViewAdapter(items: ArrayList<Venue>, ctx: Context) :
    ArrayAdapter<Venue>(ctx, R.layout.venue, items) {
    private class VenueItemViewHolder {
        internal var name: TextView? = null
        internal var location: TextView? = null
        internal var cost: TextView? = null
        internal var website: TextView? = null
        internal var phone_number: TextView? = null
        internal var constact_person: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView

        val viewHolder: VenueItemViewHolder

        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.venue, parent, false)

            viewHolder = VenueItemViewHolder()
            viewHolder.name = view.findViewById(R.id.venueName) as TextView
            viewHolder.location = view.findViewById(R.id.venueLocation) as TextView
            viewHolder.cost = view.findViewById(R.id.venueCost) as TextView
            viewHolder.website = view.findViewById(R.id.venueWebsite) as TextView
            viewHolder.phone_number = view.findViewById(R.id.venuePhoneNumber) as TextView
            viewHolder.constact_person = view.findViewById(R.id.venueContactPerson) as TextView
        }
        else {
            viewHolder = view.tag as VenueItemViewHolder
        }

        val venue = getItem(position)

        viewHolder.name!!.text = venue.name
        viewHolder.location!!.text = venue.location
        viewHolder.cost!!.text = "$" + venue.cost
        viewHolder.website!!.text = venue.website
        viewHolder.phone_number!!.text = "Phone: " + venue.phone_number
        viewHolder.constact_person!!.text = "Contact: " + venue.contact_person

        view!!.tag = viewHolder

        return view
    }
}