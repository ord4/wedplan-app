package com.example.wedplan

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject

class CreateTask : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_task)

        val TAG = "CreateTask"

        val actionBar = supportActionBar
        actionBar!!.title = "Create Task"

        val createAPIEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/tasks"
        val updateAPIEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/tasks/" + RequestSingleton.getInstance(this).weddingDocKey
        val deleteAPIEndpoint = updateAPIEndpoint

        val createTaskBtn = findViewById<Button>(R.id.createTaskBtn)
        val deleteTaskBtn = findViewById<Button>(R.id.deleteTaskBtn)
        deleteTaskBtn.visibility = View.GONE

        val title = findViewById<EditText>(R.id.createTaskTitle)
        var due_date = findViewById<DatePicker>(R.id.createTaskDueDate)
        val assignee = findViewById<EditText>(R.id.createTaskAssignee)
        val description = findViewById<EditText>(R.id.createTaskDescription)

        val updateFlag = intent.extras!!.getBoolean("updateFlag")

        val originalTask = JSONObject()
        val json = JSONObject()

        if (updateFlag) {
            Log.i(TAG, "Update")
            deleteTaskBtn.visibility = View.VISIBLE

            title.setText(intent.getStringExtra("title"), TextView.BufferType.EDITABLE)
            // FIXME: How to load in the date?
            val originalYear = intent.getStringExtra("due_date").substringAfterLast("/").toInt()
            val originalMonth = intent.getStringExtra("due_date").substringBefore("/").toInt()-1
            val originalDay = intent.getStringExtra("due_date").substringAfter("/").substringBefore("/").toInt()
            due_date.init(originalYear, originalMonth, originalDay, null)
            assignee.setText(intent.getStringExtra("assignee"), TextView.BufferType.EDITABLE)
            description.setText(intent.getStringExtra("description"), TextView.BufferType.EDITABLE)
            createTaskBtn.text = "Update Task"

            originalTask.put("title", title.text.toString())
            originalTask.put("assignee", assignee.text.toString())
            originalTask.put("description", description.text.toString())
            originalTask.put("due_date", intent.getStringExtra("due_date"))

            json.put("original", originalTask)
        }

        createTaskBtn.setOnClickListener {
            val updatedTask = JSONObject()

            val date = (due_date.month+1).toString() + '/' + due_date.dayOfMonth.toString() + '/' + due_date.year.toString()

            updatedTask.put("title", title.text)
            updatedTask.put("assignee", assignee.text)
            updatedTask.put("due_date", date)
            updatedTask.put("description", description.text)

            if (updateFlag) {
                json.put("updated", updatedTask)

                val request = JsonObjectRequest(Request.Method.PUT, updateAPIEndpoint, json,
                    Response.Listener { response ->
                        Log.i(TAG, response.toString())
                        Toast.makeText(this, "Updated your task list!", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, TasksActivity::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                        Toast.makeText(this, "Failed to update your guest list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, TasksActivity::class.java))
                    }
                )

                RequestSingleton.getInstance(this).addToRequestQueue(request)
            }
            else {
                updatedTask.put("doc_key", RequestSingleton.getInstance(this).weddingDocKey)

                val request = JsonObjectRequest(
                    Request.Method.POST, createAPIEndpoint, updatedTask,
                    Response.Listener { response ->
                        Log.i(TAG, response.toString())
                        Toast.makeText(this, "Added " + updatedTask.get("title") + " to your task list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, TasksActivity::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                        Toast.makeText(this, "Failed to add to your task list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, TasksActivity::class.java))
                    }
                )

                RequestSingleton.getInstance(this).addToRequestQueue(request)
            }
        }

        deleteTaskBtn.setOnClickListener {
            val request = JsonObjectRequest(Request.Method.DELETE, deleteAPIEndpoint+"/"+intent.extras!!.getInt("pos"), null,
                Response.Listener { response ->
                    Log.i(TAG, response.toString())
                    Toast.makeText(this, "Your task list has been updated", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, TasksActivity::class.java))
                },
                Response.ErrorListener { error ->
                    Log.d(TAG, error.toString())
                    Toast.makeText(this, "Failed to update your task list", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, TasksActivity::class.java))
                }
            )

            RequestSingleton.getInstance(this).addToRequestQueue(request)
        }
    }
}

