package com.example.wedplan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ListView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import android.content.Intent
import android.support.design.widget.FloatingActionButton
import android.widget.AdapterView

class VenuesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_venues)

        val TAG = "VenuesActivity"

        val actionBar = supportActionBar
        actionBar!!.title = "Venues"

        val apiEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/venues/" + RequestSingleton.getInstance(this).weddingDocKey

        val listView = findViewById<ListView>(R.id.venuesListView)
        listView.dividerHeight = 10

        val venueList = ArrayList<Venue>()

        val request = JsonObjectRequest(Request.Method.GET, apiEndpoint, null,
            Response.Listener { response ->
                Log.i(TAG, response.toString(2))
                val venueArray = response.getJSONArray("venues")

                for (i in 0 until venueArray.length()) {
                    val venue = venueArray.getJSONObject(i)
                    venueList.add(Venue(venue.get("name").toString(), venue.get("location").toString(), venue.get("cost").toString(), venue.get("website").toString(), venue.get("phone_number").toString(), venue.get("contact_person").toString()))
                }

                val venueAdapter = VenueListViewAdapter(venueList, this)
                listView.adapter = venueAdapter
            },
            Response.ErrorListener { error ->
                Log.d(TAG, error.toString())
            }
        )

        RequestSingleton.getInstance(this).addToRequestQueue(request)

        val addVenueButton = findViewById<FloatingActionButton>(R.id.venuesAddVenue)
        addVenueButton.setOnClickListener {
            val intent = Intent(this, CreateVenue::class.java)
            intent.putExtra("updateFlag", false)
            startActivity(intent)
        }

        listView.onItemClickListener = AdapterView.OnItemClickListener {
            adapterView, view, i, l ->
            val venue = adapterView.getItemAtPosition(i) as Venue

            val intent = Intent(this, CreateVenue::class.java)
            intent.putExtra("updateFlag", true)
            intent.putExtra("name", venue.name)
            intent.putExtra("location", venue.location)
            intent.putExtra("cost", venue.cost)
            intent.putExtra("website", venue.website)
            intent.putExtra("phone_number", venue.phone_number)
            intent.putExtra("contact_person", venue.contact_person)
            intent.putExtra("pos", i)

            startActivity(intent)
        }
    }
}
