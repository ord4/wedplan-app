package com.example.wedplan

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.*
import org.json.JSONObject
import android.util.Log

class CreateAccount : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)

        val TAG = "CreateAccount"
        val apiEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/users"

        val actionBar = supportActionBar
        actionBar!!.title = "Create Account"

        var et_fname = findViewById<EditText>(R.id.fname)
        var et_lname = findViewById<EditText>(R.id.lname)
        var et_username = findViewById<EditText>(R.id.username)
        var et_password = findViewById<EditText>(R.id.password)
        var btn_submit = findViewById<Button>(R.id.submit)

        btn_submit.setOnClickListener {
            val fname = et_fname.text
            val lname = et_lname.text
            val username = et_username.text
            val password = et_password.text

            if (fname.isBlank() || lname.isBlank() || username.isBlank() || password.isBlank()) {
                Toast.makeText(this, "Make sure to fill out all the fields", Toast.LENGTH_SHORT).show()
            }
            else {
                val req = JSONObject()
                req.put("fname", fname)
                req.put("lname", lname)
                req.put("username", username)
                req.put("password", password)

                val jsonRequest = JsonObjectRequest(Request.Method.POST, apiEndpoint, req,
                    Response.Listener { response ->
                        Log.i(TAG, response.get("_key").toString())
                        RequestSingleton.getInstance(this).userKey = response.get("_key").toString()
                        startActivity(Intent(this, CreateWedding::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                        Toast.makeText(this, "Failed to create account, please try again", Toast.LENGTH_SHORT).show()
                    }
                )

                RequestSingleton.getInstance(this).addToRequestQueue(jsonRequest)
            }
        }
    }
}
