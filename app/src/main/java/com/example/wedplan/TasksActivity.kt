package com.example.wedplan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.widget.ListView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import android.util.Log
import android.content.Intent
import android.widget.AdapterView

class TasksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tasks)

        val TAG = "TasksActivity"

        val actionBar = supportActionBar
        actionBar!!.title = "Tasks"

        val apiEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/tasks/" + RequestSingleton.getInstance(this).weddingDocKey

        val listView = findViewById<ListView>(R.id.tasksListView)
        listView.dividerHeight = 10

        val taskList = ArrayList<Task>()

        val request = JsonObjectRequest(Request.Method.GET, apiEndpoint, null,
            Response.Listener { response ->
                Log.i(TAG, response.toString(2))
                val taskArray = response.getJSONArray("tasks")

                for (i in 0 until taskArray.length()) {
                    val task = taskArray.getJSONObject(i)
                    Log.i(TAG, task.toString(2))
                    taskList.add(Task(task.get("title").toString(), task.get("due_date").toString(), task.get("assignee").toString(), task.get("description").toString()))
                }

                Log.i(TAG, taskList.toString())

                val taskAdapter = TaskListViewAdapter(taskList, this)
                listView.adapter = taskAdapter
            },
            Response.ErrorListener { error ->
                Log.d(TAG, error.toString())
            }
        )

        RequestSingleton.getInstance(this).addToRequestQueue(request)

        val addTaskBtn = findViewById<FloatingActionButton>(R.id.tasksAddTask)
        addTaskBtn.setOnClickListener {
            val intent = Intent(this, CreateTask::class.java)
            intent.putExtra("updateFlag", false)
            startActivity(intent)
        }

        listView.onItemClickListener = AdapterView.OnItemClickListener {
            adapterView, view, i, l ->

            val task = adapterView.getItemAtPosition(i) as Task

            val intent = Intent(this, CreateTask::class.java)
            intent.putExtra("updateFlag", true)
            intent.putExtra("title", task.title)
            intent.putExtra("due_date", task.due_date)
            intent.putExtra("assignee", task.assignee)
            intent.putExtra("description", task.description)
            intent.putExtra("pos", i)

            startActivity(intent)
        }
    }
}
