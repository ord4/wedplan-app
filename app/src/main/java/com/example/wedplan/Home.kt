package com.example.wedplan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.android.volley.*
import com.android.volley.toolbox.*
import android.util.Log
import android.widget.*
import android.content.Intent

class Home : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val TAG = "Home"
        val apiEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/weddings/" + RequestSingleton.getInstance(this).weddingDocKey

        val actionBar = supportActionBar
        actionBar!!.title = "Home"

        val request = JsonObjectRequest(Request.Method.GET, apiEndpoint, null,
            Response.Listener { response ->
                Log.i(TAG, response.toString())

                // Display information to the user
                val weddingName = findViewById<TextView>(R.id.homeWeddingName)
                val weddingDate = findViewById<TextView>(R.id.homeWeddingDate)
                weddingName.text = response.get("name").toString()
                weddingDate.text = response.get("date").toString()
            },
            Response.ErrorListener { error ->
                Log.i(TAG, error.toString())
            }
        )

        RequestSingleton.getInstance(this).addToRequestQueue(request)

        val guestsBtn = findViewById<ImageButton>(R.id.homeGuestsBtn)
        val tasksBtn = findViewById<ImageButton>(R.id.homeTasksBtn)
        val venuesBtn = findViewById<ImageButton>(R.id.homeVenuesBtn)

        guestsBtn.setOnClickListener {
            startActivity(Intent(this, GuestsActivity::class.java))
        }

        tasksBtn.setOnClickListener {
            startActivity(Intent(this, TasksActivity::class.java))
        }

        venuesBtn.setOnClickListener {
            startActivity(Intent(this, VenuesActivity::class.java))
        }
    }
}
