package com.example.wedplan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Context
import android.widget.EditText
import android.widget.Button
import android.util.Log
import android.widget.Toast
import android.content.Intent
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.Toolbar
import com.android.volley.*
import com.android.volley.toolbox.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val TAG = "MainActivity"
        val apiEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/users/auth"

        var et_username = findViewById<EditText>(R.id.username)
        var et_password = findViewById<EditText>(R.id.password)
        var btn_log_in = findViewById<Button>(R.id.login)
        var btn_create_account = findViewById<Button>(R.id.create_account)

        btn_log_in.setOnClickListener {
            val username = et_username.text
            val password = et_password.text

            val req = JSONObject()
            req.put("username", username)
            req.put("password", password)

            val jsonRequest = JsonObjectRequest(Request.Method.POST, apiEndpoint, req,
                Response.Listener { response ->
                    Log.i(TAG, "Sending user to home screen")
                    // FIXME: Find the wedding doc the user is associated with and store the key
                    RequestSingleton.getInstance(this).userKey = response.get("_key").toString()
                    RequestSingleton.getInstance(this).weddingDocKey = response.get("wedding_doc").toString()
                    startActivity(Intent(this, Home::class.java))
                },
                Response.ErrorListener { error ->
                    Log.d(TAG, error.toString())
                    Toast.makeText(this, "Failed to authenticate", Toast.LENGTH_SHORT).show()
                }
            )

            RequestSingleton.getInstance(this).addToRequestQueue(jsonRequest)
        }

        btn_create_account.setOnClickListener {
            startActivity(Intent(this, CreateAccount::class.java))
        }
    }
}

class RequestSingleton constructor(context: Context) {
    companion object {
        @Volatile
        private var INSTANCE: RequestSingleton? = null
        fun getInstance(context: Context) =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: RequestSingleton(context).also {
                    INSTANCE = it
                }
            }
    }
    val requestQueue: RequestQueue by lazy {
        // applicationContext is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        Volley.newRequestQueue(context.applicationContext)
    }
    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }

    var userKey = ""
    var weddingDocKey = ""

    val serverLocation = "http://192.168.1.12:5000"
}