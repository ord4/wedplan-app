package com.example.wedplan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import org.json.JSONObject
import com.android.volley.*
import com.android.volley.toolbox.*
import android.content.Intent

class CreateWedding : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_wedding)

        val TAG = "CreateWedding"
        val apiEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/weddings"

        val actionBar = supportActionBar
        actionBar!!.title = "Create Wedding"

        val weddingName = findViewById<EditText>(R.id.weddingName)
        val weddingLocation = findViewById<EditText>(R.id.weddingLocation)
        val weddingDate = findViewById<DatePicker>(R.id.weddingDate)
        val weddingCreate = findViewById<Button>(R.id.weddingCreate)

        weddingCreate.setOnClickListener {
            val name = weddingName.text

            // FIXME: Make optional fields for location, date and planners so that no "wrong" info is entered
            val location = weddingLocation.text
            val date = (weddingDate.month+1).toString() + '/' + weddingDate.dayOfMonth.toString() + '/' + weddingDate.year.toString()

            if (name.isBlank()) {
                Toast.makeText(this, "Please fill in the required fields denoted with a *", Toast.LENGTH_SHORT).show()
            }
            else {
                val json = JSONObject()
                json.put("creator", RequestSingleton.getInstance(this).userKey)
                json.put("name", name)
                json.put("location", location)
                json.put("date", date)

                val request = JsonObjectRequest(Request.Method.POST, apiEndpoint, json,
                    Response.Listener { response ->
                        RequestSingleton.getInstance(this).weddingDocKey = response.get("_key").toString()
                        Log.i(TAG, RequestSingleton.getInstance(this).weddingDocKey)
                        startActivity(Intent(this, Home::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                    }
                )

                RequestSingleton.getInstance(this).addToRequestQueue(request)
            }
        }
    }
}
