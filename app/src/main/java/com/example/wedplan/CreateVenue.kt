package com.example.wedplan

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject

class CreateVenue : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_venue)

        val TAG = "CreateVenue"

        val actionBar = supportActionBar
        actionBar!!.title = "Create Venue"

        val createAPIEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/venues"
        val updateAPIEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/venues/" + RequestSingleton.getInstance(this).weddingDocKey
        val deleteAPIEndpoint = updateAPIEndpoint

        val createVenueBtn = findViewById<Button>(R.id.createVenueBtn)
        val deleteVenueBtn = findViewById<Button>(R.id.deleteVenueBtn)
        deleteVenueBtn.visibility = View.GONE

        val name = findViewById<EditText>(R.id.createVenueName)
        val location = findViewById<EditText>(R.id.createVenueLocation)
        val cost = findViewById<EditText>(R.id.createVenueCost)
        val website = findViewById<EditText>(R.id.createVenueWebsite)
        val phone_number = findViewById<EditText>(R.id.createVenuePhoneNumber)
        val contact_person = findViewById<EditText>(R.id.createVenueContactPerson)

        val updateFlag = intent.extras!!.getBoolean("updateFlag")

        val originalVenue = JSONObject()
        val json = JSONObject()

        if (updateFlag) {
            deleteVenueBtn.visibility = View.VISIBLE

            name.setText(intent.getStringExtra("name"), TextView.BufferType.EDITABLE)
            location.setText(intent.getStringExtra("location"), TextView.BufferType.EDITABLE)
            cost.setText(intent.getStringExtra("cost"), TextView.BufferType.EDITABLE)
            website.setText(intent.getStringExtra("website"), TextView.BufferType.EDITABLE)
            phone_number.setText(intent.getStringExtra("phone_number"), TextView.BufferType.EDITABLE)
            contact_person.setText(intent.getStringExtra("contact_person"), TextView.BufferType.EDITABLE)


            originalVenue.put("name", name.text.toString())
            originalVenue.put("location", location.text.toString())
            originalVenue.put("cost", cost.text.toString())
            originalVenue.put("website", website.text.toString())
            originalVenue.put("phone_number", phone_number.text.toString())
            originalVenue.put("contact_person", contact_person.text.toString())

            createVenueBtn.text = "Update Venue"

            json.put("original", originalVenue)
        }

        createVenueBtn.setOnClickListener {
            val updatedVenue = JSONObject()

            updatedVenue.put("name", name.text)
            updatedVenue.put("location", location.text)
            updatedVenue.put("cost", cost.text)
            updatedVenue.put("website", website.text)
            updatedVenue.put("phone_number", phone_number.text)
            updatedVenue.put("contact_person", contact_person.text)

            if (updateFlag) {
                json.put("updated", updatedVenue)

                val request = JsonObjectRequest(Request.Method.PUT, updateAPIEndpoint, json,
                    Response.Listener { response ->
                        Log.i(TAG, response.toString())
                        Toast.makeText(this, "Updated your venue list!", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, VenuesActivity::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                        Toast.makeText(this, "Failed to update your venue list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, VenuesActivity::class.java))
                    }
                )

                RequestSingleton.getInstance(this).addToRequestQueue(request)
            }
            else {
                updatedVenue.put("doc_key", RequestSingleton.getInstance(this).weddingDocKey)

                val request = JsonObjectRequest(
                    Request.Method.POST, createAPIEndpoint, updatedVenue,
                    Response.Listener { response ->
                        Log.i(TAG, response.toString())
                        Toast.makeText(this, "Added " + updatedVenue.get("name") + " to your task list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, VenuesActivity::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                        Toast.makeText(this, "Failed to add to your task list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, VenuesActivity::class.java))
                    }
                )

                RequestSingleton.getInstance(this).addToRequestQueue(request)
            }
        }

        deleteVenueBtn.setOnClickListener {
            val request =
                JsonObjectRequest(Request.Method.DELETE, deleteAPIEndpoint + "/" + intent.extras!!.getInt("pos"), null,
                    Response.Listener { response ->
                        Log.i(TAG, response.toString())
                        Toast.makeText(this, "Your venue list has been updated", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, VenuesActivity::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                        Toast.makeText(this, "Failed to update your venue list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, VenuesActivity::class.java))
                    }
                )

            RequestSingleton.getInstance(this).addToRequestQueue(request)
        }
    }
}
