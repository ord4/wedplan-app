package com.example.wedplan

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.android.volley.toolbox.*
import com.android.volley.*
import org.json.JSONObject
import android.util.Log
import android.content.Intent
import android.text.Editable
import android.view.View
import android.widget.*

class CreateGuest : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_guest)

        val TAG = "CreateGuest"

        val actionBar = supportActionBar
        actionBar!!.title = "Create Guest"

        val createAPIEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/guests"
        val updateAPIEndpoint = RequestSingleton.getInstance(this).serverLocation + "/wedplan/api/v0.1/guests/" + RequestSingleton.getInstance(this).weddingDocKey
        val deleteAPIEndpoint = updateAPIEndpoint

        val createGuestBtn = findViewById<Button>(R.id.createGuestBtn)
        val createGuestDelBtn = findViewById<Button>(R.id.createGuestDelBtn)
        createGuestDelBtn.visibility = View.GONE

        val fname =findViewById<EditText>(R.id.createGuestFname)
        val lname = findViewById<EditText>(R.id.createGuestLname)
        val plus_one = findViewById<CheckBox>(R.id.createGuestPlusOne)

        val updateFlag = intent.extras!!.getBoolean("updateFlag")

        val originalGuest = JSONObject()
        val json = JSONObject()

        if (updateFlag) {
            createGuestDelBtn.visibility = View.VISIBLE
            fname.setText(intent.getStringExtra("fname"), TextView.BufferType.EDITABLE)
            lname.setText(intent.getStringExtra("lname"), TextView.BufferType.EDITABLE)
            plus_one.isChecked = intent.extras!!.getBoolean("plus_one")
            createGuestBtn.text = "Update Guest"

            // Store our original entry
            originalGuest.put("fname", fname.text.toString())
            originalGuest.put("lname", lname.text).toString()

            if (plus_one.isChecked) {
                originalGuest.put("plus_one", true)
                originalGuest.put("count", 2)
            }

            else {
                originalGuest.put("plus_one", false)
                originalGuest.put("count", 1)
            }

            json.put("original", originalGuest)
            Log.i(TAG, json.toString(2))
        }

        createGuestBtn.setOnClickListener {
            val updatedGuest = JSONObject()

            updatedGuest.put("fname", fname.text)
            updatedGuest.put("lname", lname.text)

            if (plus_one.isChecked) {
                updatedGuest.put("plus_one", true)
                updatedGuest.put("count", 2)
            }

            else {
                updatedGuest.put("plus_one", false)
                updatedGuest.put("count", 1)
            }

            if (updateFlag) {
                updatedGuest.remove("doc_key")

                json.put("updated", updatedGuest)

                Log.i(TAG, json.toString(2))

                val request = JsonObjectRequest(Request.Method.PUT, updateAPIEndpoint, json,
                    Response.Listener { response ->
                        Log.i(TAG, response.toString())
                        Toast.makeText(this, "Updated your guest list!", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, GuestsActivity::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                        Toast.makeText(this, "Failed to update your guest list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, GuestsActivity::class.java))
                    }
                )

                RequestSingleton.getInstance(this).addToRequestQueue(request)
            }
            else {
                updatedGuest.put("doc_key", RequestSingleton.getInstance(this).weddingDocKey)

                val request = JsonObjectRequest(Request.Method.POST, createAPIEndpoint, updatedGuest,
                    Response.Listener { response ->
                        Log.i(TAG, response.toString())
                        Toast.makeText(this, "Added " + updatedGuest.get("fname") + " to your guest list!", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, GuestsActivity::class.java))
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, error.toString())
                        Toast.makeText(this, "Failed to add to your guest list", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, GuestsActivity::class.java))
                    }
                )

                RequestSingleton.getInstance(this).addToRequestQueue(request)
            }

        }

        createGuestDelBtn.setOnClickListener {
            val request = JsonObjectRequest(Request.Method.DELETE, deleteAPIEndpoint+"/"+intent.extras!!.getInt("pos"), null,
                Response.Listener { response ->
                    Log.i(TAG, response.toString())
                    Toast.makeText(this, "Your guest list has been updated", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, GuestsActivity::class.java))
                },
                Response.ErrorListener { error ->
                    Log.d(TAG, error.toString())
                    Toast.makeText(this, "Failed to update your guest list", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, GuestsActivity::class.java))
                }
            )

            RequestSingleton.getInstance(this).addToRequestQueue(request)
        }
    }
}
